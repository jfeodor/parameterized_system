import json
import logging
import os
import sys
from time import sleep
import typing
from dataclasses import dataclass

import grpc
from grpc_interceptor import ClientCallDetails, ClientInterceptor

import spm_pb2
import spm_pb2_grpc

class Interceptor(ClientInterceptor):
    def __init__(self, access_token: str):
        self._access_token = access_token

    def intercept(self, method: typing.Callable, request_or_iterator: typing.Any, call_details: grpc.ClientCallDetails):
        new_details = ClientCallDetails(
            call_details.method,
            call_details.timeout,
            [("token", self._access_token)],
            call_details.credentials,
            call_details.wait_for_ready,
            call_details.compression,
        )

        return method(request_or_iterator, new_details)


@dataclass
class LaunchContext:
    refresh_token: str
    launch_id: str
    project_id: str
    scenario_id: str
    job_id: str
    my_input: str


def grpc_channel(api_host: str, api_port: str, force_insecure: bool):
    options: dict[str, typing.Any] = {}
    if force_insecure:
        return grpc.insecure_channel(f'{api_host}:{api_port}', options)
    else:
        return grpc.secure_channel(f'{api_host}:{api_port}', grpc.ssl_channel_credentials(), options)


def spm_client(channel: grpc.Channel):
    return spm_pb2_grpc.SPMStub(channel)


def token_client(channel: grpc.Channel):
    return spm_pb2_grpc.TokenManagerStub(channel)


def get_access_token(token_client: spm_pb2_grpc.TokenManagerStub, context: LaunchContext):
    access_token_request = spm_pb2.RefreshRequest(
        refresh_token=spm_pb2.Token(val=context.refresh_token),
        execution_id=context.launch_id,
        project_id=context.project_id,
        scenario_id=context.scenario_id,
    )
    access_token = token_client.GetAccessToken(access_token_request)
    return str(access_token.val)


def main(spm_client: spm_pb2_grpc.SPMStub, context: LaunchContext):
    spm_client.SetScenarioProgress(
        request=spm_pb2.ScenarioProgress(
            project=context.project_id,
            scenario=context.scenario_id,
            job_id=context.job_id,
            message=f'My input was: {context.my_input}',
            status='running',
            progress=0.0
        )
    )
    sleep(10)


def extract_parameter(serialized_parameters: str) -> str:
    parameters = json.loads(serialized_parameters)

    if not isinstance(parameters, list):
        raise TypeError('Parameters must be a list')

    for param in parameters:
        if not isinstance(param, dict):
            raise TypeError('Parameter must be a dict')

        if param['name'] == 'My Input':
            return param['value']

    raise ValueError('Missing required parameter')


if __name__ == '__main__':
    api_host = os.environ['NUMEROUS_API_HOST']
    api_port = os.environ['NUMEROUS_API_PORT']
    force_insecure = os.getenv('FORCE_INSECURE', 'False') == 'True'
    launch_context = LaunchContext(
        refresh_token=os.environ['NUMEROUS_REFRESH_TOKEN'],
        launch_id=os.environ['NUMEROUS_LAUNCH_ID'],
        project_id=os.environ['NUMEROUS_PROJECT_ID'],
        scenario_id=os.environ['NUMEROUS_SCENARIO_ID'],
        job_id=os.environ['NUMEROUS_JOB_ID'],
        my_input=extract_parameter(os.environ['NUMEROUS_LAUNCH_PARAMETERS'])
    )

    channel = grpc_channel(api_host, api_port, force_insecure)
    access_token = get_access_token(token_client(channel), launch_context)
    channel = grpc.intercept_channel(channel, Interceptor(access_token))

    try:
        main(spm_client=spm_client(channel), context=launch_context)
    except:
        logging.exception('Job failed')
        sys.exit(1)
    else:
        sys.exit(0)

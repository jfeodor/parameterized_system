FROM python:3.9
ARG BOOTSTRAP_CONTEXT_DIRECTORY
ARG HYDRATED_SYSTEM

RUN pip install --extra-index-url=https://pypi.numerously.com/simple numerous_api_client==0.16.5
COPY ${BOOTSTRAP_CONTEXT_DIRECTORY}/script.py /script.py
ENTRYPOINT [ "python", "/script.py" ]
